from django.conf.urls import url, include
from django.contrib import admin

from shop.views import index


urlpatterns = [
    url(r'^$', index),
    url(r'^admin/', admin.site.urls),
    url('^accounts/', admin.site.urls),
]

admin.site.site_header = 'Мой магазин'