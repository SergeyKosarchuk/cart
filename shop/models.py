from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now


class Product(models.Model):
    vendor_code = models.CharField(
        verbose_name='Артикул', max_length=32, unique=True)
    name = models.CharField('Название', max_length=100)
    quantity = models.PositiveSmallIntegerField('Кол-во')
    price = models.DecimalField(
        verbose_name='Цена за шт.', max_digits=5, decimal_places=2)

    def __str__(self):
        return f'{self.name} {self.vendor_code}'

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'


class Order(models.Model):
    number = models.CharField(verbose_name='Номер', unique=True, max_length=32)
    date = models.DateTimeField('Дата заказа', default=now)
    user = models.ForeignKey(User, models.CASCADE, verbose_name='Пользователь')
    products = models.ManyToManyField(Product, through='ProductToOrder')

    def __str__(self):
        return self.number

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'


class ProductToOrder(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Товар')
    quantity = models.PositiveSmallIntegerField(verbose_name='Кол-во')
    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
    
    def __str__(self):
        return ''
