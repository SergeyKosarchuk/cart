from django.contrib import admin
from django.contrib.admin import AdminSite
from django.forms import ModelForm
from .models import Order, Product, ProductToOrder
from django.contrib.auth.models import User, Group, Permission
from django.utils.translation import ugettext_lazy as _


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('vendor_code', 'name', 'price', 'quantity',)


class ProductInlineAdmin(admin.TabularInline):
    model = ProductToOrder
    show_change_link = False
    verbose_name = _('Товар')
    min_num = 1
    extra = 0


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'number', 'date', 'user', 'display_quantity', 'display_total_cost',)
    inlines = [ProductInlineAdmin]

    fieldsets = (
        (None, {
            'fields': ('number', 'date',)
        }),
    )

    restricted_fieldsets = (
        (None, {
            'fields': ('number', 'date',)
        }),
        ('Пользователь', {
            'fields': ('user',)
        })
    )

    def get_fieldsets(self, request, obj=None):
        if request.user.is_superuser:
            return self.restricted_fieldsets
        else:
            return super(OrderAdmin, self).get_fieldsets(request, obj=obj)

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return self.readonly_fields
        return self.readonly_fields + ('user',)


    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            obj.user = request.user
            obj.save()
        super(OrderAdmin, self).save_model(request, obj, form, change)

    def display_total_cost(self, obj: Order):  # noqa: pylint: no-self-use
        total = 0   
        products_in_order = ProductToOrder.objects.filter(order=obj)
        for pr in products_in_order:
            total = pr.quantity * pr.product.price
        return total


    display_total_cost.allow_tags = True
    display_total_cost.short_description = _('Общая стоймость')

    def display_quantity(self, obj: Order):  # noqa: pylint: no-self-use
        return ProductToOrder.objects.filter(order=obj).count()

    display_quantity.allow_tags = True
    display_quantity.short_description = _('Кол-во позиций')
