from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required
def index(request):
    from django.shortcuts import render, redirect
    if request.user.is_staff:
        return redirect('/admin/')
    return render(request, 'access_denied.html', {})